#include "../Tree/DecisionTree.h"
#include "../Tree/Factories/DecisionTreeLeafFactory.h"
#include <cstdlib>
#include <vector>

#ifndef SORTALGORITHM_H_INCLUDED
#define SORTALGORITHM_H_INCLUDED

using namespace std;

class SortAlgorithm
{
public:
	SortAlgorithm(const string& type);
	void execute(vector<int>& v, int firstElementIndex, int lastElementIndex);
	virtual void sort(vector<int>& v, int firstElementIndex, int lastElementIndex) = 0;
	
	~SortAlgorithm();
	
	inline void printTree() { root->print(); }
protected:
	DecisionTree* root;	
	DecisionTree* currTree;
	DecisionTreeLeafFactory* factory;
	vector<int> currPermutation;

	//return true if v[i] < v[j] and push the subtree correctly
	bool compare(vector<int>& v, int i, int j);

	string intToString(int number);
	string printVector(const vector<int>& v);
};

#endif