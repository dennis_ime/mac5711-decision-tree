#include "Selectionsort.h"

Selectionsort::Selectionsort(const string& type)
	:SortAlgorithm(type)
{}

Selectionsort::~Selectionsort()
{}

void Selectionsort::sort(vector<int>& v, int firstElementIndex, int lastElementIndex)
{
	currPermutation = vector<int>(v);
	selectionsort(currPermutation);
}

void Selectionsort::swap(vector<int> &v, int i, int j)
{
	auto temp = v[i];
	v[i] = v[j];
	v[j] = temp;
}

void Selectionsort::selectionsort(vector<int> v)
{
	int i, j, min;
	
	for(i = 0; i < (v.size()-1); i++)
	{
		min = i;
		for(j = (i+1); j < v.size(); j++)
		{
			if(compare(v, j, min))
				min = j;
		}

		if(i != min)
			swap(v, i, min);
	}
}