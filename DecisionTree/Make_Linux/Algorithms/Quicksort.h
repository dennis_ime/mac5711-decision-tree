#include "SortAlgorithm.h"
#include <iostream>

#ifndef QUICKSORT_H_INCLUDED
#define QUICKSORT_H_INCLUDED

using namespace std;

class Quicksort: public SortAlgorithm
{
public:
	Quicksort(const string& type);
	~Quicksort();

	void sort(vector<int>& v, int firstElementIndex, int lastElementIndex);
private:
	void swap(vector<int>& v, int i, int j);
	int partiton(vector<int>& v, int l, int r);
	void quicksort(vector<int>& v, int l, int r);
};

#endif