#include "SortAlgorithm.h"
#include <iostream>

SortAlgorithm::SortAlgorithm(const string& type)
{
	factory = DecisionTreeLeafFactory::getFactory(type);
	root = nullptr;
	currTree = nullptr;
}

SortAlgorithm::~SortAlgorithm()
{
	delete factory;
}

string SortAlgorithm::intToString(int number)
{
	char buffer[33];
	sprintf(buffer, "%d", number);
	
	return string(buffer);
}

string SortAlgorithm::printVector(const vector<int>& v)
{
	string strVector = string();

	for(auto itr = v.begin(); itr != v.end(); ++itr)
		strVector += intToString(*itr);

	return strVector;
}

bool SortAlgorithm::compare(vector<int>& v, int i, int j)
{
	//Left Subtree
	if(v[i] < v[j])
	{
		if(root == nullptr)
		{
			root = currTree = factory->create(printVector(currPermutation));
			return true;
		}
		else
		{
			if(currTree->getLeftSubTree() == nullptr)
			{
				currTree->setLabel(intToString(i) + ":" + intToString(j));
				currTree->setLeftTree(factory->create(printVector(currPermutation)));
			}
			currTree = currTree->getLeftSubTree();
			return true;
		}
	}
	//Right Subtree
	else
	{
		if(root == nullptr)
		{
			root = currTree = factory->create(printVector(currPermutation));
			return false;
		}
		else
		{
			if(currTree->getRightSubTree() == nullptr)
			{
				currTree->setLabel(intToString(i) + ":" + intToString(j));
				currTree->setRightTree(factory->create(printVector(currPermutation)));
			}
			currTree = currTree->getRightSubTree();
			return false;
		}
	}
}

void SortAlgorithm::execute(vector<int>& v, int firstElementIndex, int lastElementIndex)
{
	if(root == nullptr)
		root = factory->create(printVector(v));
	
	currTree = root;
	sort(v, firstElementIndex, lastElementIndex);
}
