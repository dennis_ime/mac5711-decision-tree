#include "SortAlgorithm.h"

#ifndef SELECTIONSORT_H_INCLUDED
#define SELECTIONSORT_H_INCLUDED

class Selectionsort: public SortAlgorithm
{
public:
	Selectionsort(const string& type);
	~Selectionsort();

	void sort(vector<int>& v, int firstElementIndex, int lastElementIndex);


private:
	void swap(vector<int> &v, int i, int j);
	void selectionsort(vector<int> v);
};

#endif