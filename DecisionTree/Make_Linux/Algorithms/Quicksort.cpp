#include "Quicksort.h"

Quicksort::Quicksort(const string& type):
	SortAlgorithm(type)
{}

Quicksort::~Quicksort()
{}

void Quicksort::sort(vector<int>& v, int firstElementIndex, int lastElementIndex)
{
	currPermutation = vector<int>(v);
	quicksort(v, firstElementIndex, lastElementIndex-1);
}

void Quicksort::quicksort(vector<int>& v, int l, int r)
{
	if(r <= l) 
		return;

	auto i = partiton(v, l, r);
	quicksort(v, l, i-1);
	quicksort(v, i+1, r);
}

int Quicksort::partiton(vector<int>& v, int l, int r)
{
	int i = l, j = r - 1;
	while(true)
	{
		while(compare(v, i, r))
			i++;
		while(j < l && compare(v, r, j))
			j--;

		if(i >= j) 
			break;
		swap(v, i, j);
		i++;
		j--;
	}

	swap(v, i, r);
	return i;
}

void Quicksort::swap(vector<int>& v, int i, int j)
{
	auto temp = v[i];
	v[i] = v[j];
	v[j] = temp;
}