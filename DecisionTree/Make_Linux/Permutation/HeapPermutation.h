#include <vector>

#ifndef HEAPPERMUTATION_H_INCLUDED
#define HEAPPERMUTATION_H_INCLUDED

using namespace std;

template<class T>
class HeapPermutation
{
protected:
	virtual void executePermutation(vector<T>& permutation) = 0; 
	void heap(vector<T>& toPermute, int n);

	void swap(vector<T>& v, int i, int j);
	bool isOdd(int number);

public:
	void execute(vector<T>& toPermute);
};


template<class T>
void HeapPermutation<T>::execute(vector<T>& toPermute)
{
	heap(toPermute, toPermute.size());
}

template<class T>
void HeapPermutation<T>::swap(vector<T>& v, int i, int j)
{
	auto temp = v[i];
	v[i] = v[j];
	v[j] = temp;
}

template<class T>
bool HeapPermutation<T>::isOdd(int number)
{
	return (number % 2) == 1;	
}

template<class T>
void HeapPermutation<T>::heap(vector<T>& toPermute, int n)
{
	int i;
	if(n == 1)
		executePermutation(toPermute);
	else
	{
		for(i = 0; i < n; i++)
		{
			heap(toPermute, n-1);
			if(isOdd(n))
				swap(toPermute, 0, n-1);
			else
				swap(toPermute, i, n-1);
		}
	}
}

#endif