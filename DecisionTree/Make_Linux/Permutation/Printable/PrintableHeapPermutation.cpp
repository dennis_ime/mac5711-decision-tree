#include "PrintableHeapPermutation.h"

void PrintableHeapPermutation::executePermutation(vector<int>& permutation)
{
	for(auto itr = permutation.begin(); itr != permutation.end(); ++itr)
		cout << *itr;
	cout << endl;
}
