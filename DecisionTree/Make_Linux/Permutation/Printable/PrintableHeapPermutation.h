#include "../HeapPermutation.h"
#include <iostream>

#ifndef PRINTABLEHEAPPERMUTATION_H_INCLUDED
#define PRINTABLEHEAPPERMUTATION_H_INCLUDED

using namespace std;

class PrintableHeapPermutation: public HeapPermutation<int>
{
protected:
	void executePermutation(vector<int>& permutation);
};

#endif