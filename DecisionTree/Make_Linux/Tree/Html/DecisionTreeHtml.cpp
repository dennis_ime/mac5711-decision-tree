#include "DecisionTreeHtml.h"

ofstream* DecisionTreeHtml::file = new ofstream();

DecisionTreeHtml::DecisionTreeHtml(const string& p_label, DecisionTree* p_leftTree, DecisionTree* p_rightTree)
	:DecisionTree(p_label, p_leftTree, p_rightTree)
{}

DecisionTreeHtml::DecisionTreeHtml(const string& p_label)
	:DecisionTree(p_label, nullptr, nullptr)
{}


DecisionTreeHtml::~DecisionTreeHtml()
{
	file->close();
}

void DecisionTreeHtml::internalPrint()
{
	if(leftTree == nullptr && rightTree == nullptr)
		*file << "{id: " << id << ", pid:" << pid << ", label:\"" << label << "\"},";
	else
	{
		*file << "{id: " << id << ", pid:" << pid << ", label:\"" << label << "\"},";
		if(leftTree != nullptr)
			leftTree->internalPrint();
		
		if(rightTree != nullptr)
			rightTree->internalPrint();
	}
}

void DecisionTreeHtml::print()
{
	file->open("decisionTree.js");
	*file << "var decisionTree = [";
	internalPrint();
	*file << "]";
}

