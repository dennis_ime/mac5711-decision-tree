#include "../DecisionTree.h"
#include <iostream>
#include <fstream>

#ifndef DECISIONTREEHTML_H_INCLUDED
#define DECISIONTREEHTML_H_INCLUDED

using namespace std;

class DecisionTreeHtml: public DecisionTree
{
private:
	static ofstream* file;

public:
	DecisionTreeHtml(const string& p_label, DecisionTree *p_leftTree, DecisionTree* p_rightTree);
	DecisionTreeHtml(const string& p_label);
	~DecisionTreeHtml();

	void internalPrint();

	void print();
};

#endif