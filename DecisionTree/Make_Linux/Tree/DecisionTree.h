#include <string>

#ifndef DECISIONTREE_H_INCLUDED
#define DECISIONTREE_H_INCLUDED

using namespace std;

class DecisionTree 
{
protected:	
	static int idCount;
	int id;
	int pid;
	string label;
	DecisionTree* leftTree;
	DecisionTree* rightTree;	

	

public:
	DecisionTree(const string& p_label, DecisionTree* p_leftTree, DecisionTree* p_rightTree);
	~DecisionTree();

	virtual void print() = 0;
	virtual void internalPrint() = 0;

	inline void setLeftTree(DecisionTree* subTree) { subTree->id = ++idCount; subTree->pid = id; leftTree = subTree; }
	inline void setRightTree(DecisionTree* subTree) { subTree->id = ++idCount; subTree->pid = id; rightTree = subTree; }
	inline void setLabel(const string& p_label) { label = p_label; } 
	inline DecisionTree* getLeftSubTree() { return leftTree; }
	inline DecisionTree* getRightSubTree() { return rightTree; }
	
};

#endif