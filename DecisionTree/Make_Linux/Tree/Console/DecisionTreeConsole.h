#include "../DecisionTree.h"
#include <iostream>

#ifndef DECISIONTREECONSOLE_H_INCLUDED
#define DECISIONTREECONSOLE_H_INCLUDED

using namespace std;

class DecisionTreeConsole: public DecisionTree
{
public:
	DecisionTreeConsole(const string& p_label, DecisionTree *p_leftTree, DecisionTree* p_rightTree);
	DecisionTreeConsole(const string& p_label);
	~DecisionTreeConsole();
		
	void internalPrint();	
	void print();
};

#endif