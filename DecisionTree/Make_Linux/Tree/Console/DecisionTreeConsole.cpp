#include "DecisionTreeConsole.h"

DecisionTreeConsole::DecisionTreeConsole(const string& p_label, DecisionTree* p_leftTree, DecisionTree* p_rightTree)
	:DecisionTree(p_label, p_leftTree, p_rightTree)
{}

DecisionTreeConsole::DecisionTreeConsole(const string& p_label)
	:DecisionTree(p_label, nullptr, nullptr)
{}


DecisionTreeConsole::~DecisionTreeConsole()
{}

void DecisionTreeConsole::print()
{
	internalPrint();
}

void DecisionTreeConsole::internalPrint()
{
	if(leftTree == nullptr && rightTree == nullptr)
		cout << label;
	else
	{
		cout << "(" << label << ", ";
		if(leftTree != nullptr)
			leftTree->internalPrint();
		else
		 cout << "NULL";
		 
		cout << ", ";
		if(rightTree != nullptr)
			rightTree->internalPrint();
		else
			cout << "NULL";
		
		cout << ")";
	}
}
