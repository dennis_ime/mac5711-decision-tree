#include "DecisionTreeLeafFactory.h"
#include "DecisionTreeConsoleLeafFactory.h"
#include "DecisionTreeHtmlLeafFactory.h"

DecisionTreeLeafFactory::DecisionTreeLeafFactory()
{}

DecisionTreeLeafFactory* DecisionTreeLeafFactory::getFactory(const string& factoryType)
{
	//TODO: Return Console Factory
	if(factoryType == "console")
		return new DecisionTreeConsoleLeafFactory();
	else if (factoryType == "html")
		return new DecisionTreeHtmlLeafFactory();

	return nullptr;
}