#include "DecisionTreeLeafFactory.h"
#include "../Html/DecisionTreeHtml.h"

#ifndef DECISIONTREEHTMLLEAFFACTORY_H_INCLUDED
#define DECISIONTREEHTMLLEAFFACTORY_H_INCLUDED

class DecisionTreeHtmlLeafFactory: public DecisionTreeLeafFactory
{
public:
	DecisionTreeHtmlLeafFactory();
	DecisionTree* create(const string& label);
};

#endif