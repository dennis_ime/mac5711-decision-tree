#include "Heapsort.h"

Heapsort::Heapsort(const string& type):
	SortAlgorithm(type)
{}

Heapsort::~Heapsort()
{}

void Heapsort::sort(vector<int>& v, int firstElementIndex, int lastElementIndex)
{
	currPermutation = vector<int>(v);
	heapsort(currPermutation, firstElementIndex, lastElementIndex - 1);
}

void Heapsort::heapsort(vector<int>& v, int p, int r)
{
}

void Heapsort::swap(vector<int>& v, int i, int j)
{
	auto temp = v[i];
	v[i] = v[j];
	v[j] = temp;
}