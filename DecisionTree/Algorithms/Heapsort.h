#include "SortAlgorithm.h"
#include <iostream>

#ifndef HEAPSORT_H_INCLUDED
#define HEAPSORT_H_INCLUDED

using namespace std;

class Heapsort: public SortAlgorithm
{
public:
	Heapsort(const string& type);
	~Heapsort();

	void sort(vector<int>& v, int firstElementIndex, int lastElementIndex);
private:
	void swap(vector<int>& v, int i, int j);
	void heapsort(vector<int>& v, int l, int r);
};

#endif