#include "SortAlgorithm.h"
#include <iostream>

#ifndef MERGESORT_H_INCLUDED
#define MERGESORT_H_INCLUDED

using namespace std;

class Mergesort: public SortAlgorithm
{
public:
	Mergesort(const string& type);
	~Mergesort();

	void sort(vector<int>& v, int firstElementIndex, int lastElementIndex);

	
private:
	void swap(vector<int>& v, int i, int j);
	void merge(int p, int q, int r, vector<int>& v);
	void mergesort(int p, int r, vector<int>& v);
};

#endif