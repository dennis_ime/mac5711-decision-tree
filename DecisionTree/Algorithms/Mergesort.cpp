#include "Mergesort.h"

Mergesort::Mergesort(const string& type):
	SortAlgorithm(type)
{}

Mergesort::~Mergesort()
{}

void Mergesort::sort(vector<int>& v, int firstElementIndex, int lastElementIndex)
{
	currPermutation = vector<int>(v);
	mergesort(firstElementIndex, lastElementIndex, v);
}

void Mergesort::mergesort(int p, int r, vector<int>& v)
{
	if(p < r-1)
	{
		int q = (p + r) / 2;
		mergesort(p, q, v);
		mergesort(q, r, v);
		merge(p, q, r, v);
	}
}

void Mergesort::merge(int p, int q, int r, vector<int>& v)
{
	int i = p, j = q, k = 0;
	vector<int> w(r - p);

	while(i < q && j < r)
	{
		if(compare(v, i, j))
			w[k++] = v[i++];
		else
			w[k++] = v[j++];
	}

	while(i < q) 
		w[k++] = v[i++];
	
	while(j < r)
		w[k++] = v[j++];

	for(i = p; i < r; i++)
		v[i] = w[i-p];
	
}

void Mergesort::swap(vector<int>& v, int i, int j)
{
	auto temp = v[i];
	v[i] = v[j];
	v[j] = temp;
}