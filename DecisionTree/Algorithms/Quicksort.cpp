#include "Quicksort.h"

Quicksort::Quicksort(const string& type):
	SortAlgorithm(type)
{}

Quicksort::~Quicksort()
{}

void Quicksort::sort(vector<int>& v, int firstElementIndex, int lastElementIndex)
{
	currPermutation = vector<int>(v);
	quicksort(currPermutation, firstElementIndex, lastElementIndex - 1);
}

void Quicksort::quicksort(vector<int>& v, int p, int r)
{
	int j;
	if(p < r)
	{
		j = partiton(v, p, r);
		quicksort(v, p, j-1);
		quicksort(v, j+1, r);
	}
}

int Quicksort::partiton(vector<int>& v, int p, int r)
{
	int pivot = r, i = p, j;

	for(j = p; j < r; ++j)
	{
		if(compare(v, j, pivot))
		{
			swap(v, i, j);
			++i;
		}
	}

	swap(v, i, r);

	return i;
}

void Quicksort::swap(vector<int>& v, int i, int j)
{
	auto temp = v[i];
	v[i] = v[j];
	v[j] = temp;
}