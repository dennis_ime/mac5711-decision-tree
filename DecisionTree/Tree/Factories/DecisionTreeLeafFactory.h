#include "../DecisionTree.h"

#ifndef DECISIONTREELEAFFACTORY_H_INCLUDED
#define DECISIONTREELEAFFACTORY_H_INCLUDED

class DecisionTreeLeafFactory
{
protected:
	DecisionTreeLeafFactory();

public:
	virtual DecisionTree* create(const string& label) = 0;
	static DecisionTreeLeafFactory* getFactory(const string& factoryType);
};

#endif