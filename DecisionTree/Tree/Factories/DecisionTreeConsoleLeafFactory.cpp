#include "DecisionTreeConsoleLeafFactory.h"

DecisionTreeConsoleLeafFactory::DecisionTreeConsoleLeafFactory()	
{}

DecisionTree* DecisionTreeConsoleLeafFactory::create(const string& label)
{
	return new DecisionTreeConsole(label);
}