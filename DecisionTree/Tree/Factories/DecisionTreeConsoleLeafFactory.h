#include "DecisionTreeLeafFactory.h"
#include "../Console/DecisionTreeConsole.h"

#ifndef DECISIONTREECONSOLELEAFFACTORY_H_INCLUDED
#define DECISIONTREECONSOLELEAFFACTORY_H_INCLUDED

class DecisionTreeConsoleLeafFactory: public DecisionTreeLeafFactory
{
public:
	DecisionTreeConsoleLeafFactory();
	DecisionTree* create(const string& label);
};

#endif