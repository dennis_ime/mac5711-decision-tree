#include "DecisionTreeHtmlLeafFactory.h"

DecisionTreeHtmlLeafFactory::DecisionTreeHtmlLeafFactory()	
{}

DecisionTree* DecisionTreeHtmlLeafFactory::create(const string& label)
{
	return new DecisionTreeHtml(label);
}