#include "DecisionTree.h"

int  DecisionTree::idCount = 0;

DecisionTree::DecisionTree(const string& p_label, DecisionTree* p_leftTree, DecisionTree* p_rightTree)
	:label(p_label), leftTree(p_leftTree), rightTree(p_rightTree), id(0), pid(-1)
{}

DecisionTree::~DecisionTree()
{
	delete leftTree;
	leftTree = nullptr;
	
	delete rightTree;
	rightTree = nullptr;
}