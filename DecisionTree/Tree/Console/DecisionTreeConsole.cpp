#include "DecisionTreeConsole.h"

DecisionTreeConsole::DecisionTreeConsole(const string& p_label, DecisionTree* p_leftTree, DecisionTree* p_rightTree)
	:DecisionTree(p_label, p_leftTree, p_rightTree)
{}

DecisionTreeConsole::DecisionTreeConsole(const string& p_label)
	:DecisionTree(p_label, nullptr, nullptr)
{}


DecisionTreeConsole::~DecisionTreeConsole()
{}

void DecisionTreeConsole::print()
{
	internalPrint();
}

void DecisionTreeConsole::internalPrint()
{
	if(leftTree == nullptr && rightTree == nullptr)
		cout << label;
	else
	{
		cout << "(" << label << ", ";
		leftTree != nullptr? leftTree->internalPrint() : cout << "NULL";
		cout << ", ";
		rightTree != nullptr ? rightTree->internalPrint() : cout << "NULL";
		cout << ")";
	}
}