#include <iostream>
#include <cstdlib>

#include "Algorithms\Mergesort.h"
#include "Algorithms\Selectionsort.h"
#include "Algorithms\Quicksort.h"
#include "Permutation\TreeMaker\DecisionTreeMakerPermutation.h"
#include "Algorithms\Heapsort.h"

#include <fstream>

using namespace std;

int main(int argc, char *argv[])
{
	int arraySize;
	string algorithmName;
	string treeType;
	SortAlgorithm* algorithm;

	ofstream test;

	switch(argc)
	{
	case 3:
		arraySize = atoi(argv[1]);
		algorithmName = string(argv[2]);
		treeType = "html";		
		break;
	
	case 4:
		arraySize = atoi(argv[1]);
		algorithmName = string(argv[2]);
		treeType = string(argv[3]);
		break;

	default:
		cerr << "Parametros incorretos, padrao desejado e: "  << endl;
		cerr << "decisionTree n mergesort|selectionsort|quicksort|heapsort [console|html]" << endl;
		exit(1);
	}
	
	vector<int> v(arraySize);
	for(int i = 0; i < arraySize; i++)
		v[i] = i;

	if(algorithmName == "selectionsort")
		algorithm = new Selectionsort(treeType);
	else if(algorithmName == "mergesort")
		algorithm = new Mergesort(treeType);
	else if(algorithmName == "quicksort")
		algorithm = new Quicksort(treeType);
	else if(algorithmName == "heapsort")
		algorithm = new Heapsort(treeType);
	else
	{
		cerr << "Nome do algoritmo incorreto: "  << endl;
		cerr << "algoritmos disponives: mergesort|selectionsort|quicksort|heapsort" << endl;
		exit(1);
	}

	DecisionTreeMakerPermutation treeMarker(algorithm);
	treeMarker.execute(v);
	treeMarker.printResult();

	return 0;
}
