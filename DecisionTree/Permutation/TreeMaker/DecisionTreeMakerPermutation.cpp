#include "DecisionTreeMakerPermutation.h"

DecisionTreeMakerPermutation::DecisionTreeMakerPermutation(SortAlgorithm* p_sortAlgorithm)
{
	sortAlgorithm = p_sortAlgorithm;
}

DecisionTreeMakerPermutation::~DecisionTreeMakerPermutation()
{
	delete sortAlgorithm;
}



void DecisionTreeMakerPermutation::executePermutation(vector<int>& permutation)
{
	vector<int> copy(permutation);
	sortAlgorithm->execute(copy, 0, copy.size());
}