#include "../HeapPermutation.h"
#include "../../Algorithms/SortAlgorithm.h"

#ifndef DECISIONTREEMAKERPERMUTATION_H_INCLUDED
#define DECISIONTREEMAKERPERMUTATION_H_INCLUDED

class DecisionTreeMakerPermutation : public HeapPermutation<int>
{
public:
	DecisionTreeMakerPermutation(SortAlgorithm* p_sortAlgorithm);
	~DecisionTreeMakerPermutation();

	inline void printResult(){ sortAlgorithm->printTree(); }

protected:
	SortAlgorithm* sortAlgorithm;
	void executePermutation(vector<int>& permutation);
};

#endif